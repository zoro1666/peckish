import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    TouchableHighlight,
    StyleSheet,
} from 'react-native';

class Home extends Component {
    searchPressed() {
	this.props.fetchRecipes('bacon,banana');
    }
    render() {
	return <View style={{marginTop: 20}}>
	           <View>
	               <TouchableHighlight onPress={() => this.searchPressed()}>
	                   <Text>Fetch Recipes</Text>
	               </TouchableHighlight>
	           </View>
	       </View>
    }
}

function mapStateToProps(state) {
    return {
	searchedRecipes: state.searchedRecipes
    }
}

export default connect(mapStateToProps)(Home);
